global Event transition;
global Event transitionEnd;

public class Conductor extends Part {
	1 => int shouldContinue;
	[84, 88, 91, 96] @=> int tonic[];
	[93, 96, 100, 105] @=> int mediant[];
	[86, 89, 93, 98] @=> int pre_dominant[];
	[91, 95, 98, 103] @=> int dominant[];

	[tonic, mediant, pre_dominant, dominant] @=> int progression[][];
	int current;

	RhythmGenerator rgen;

	Shred @ shreds[2];

	fun int[] current_chord() {
		return progression[current];
	}

	fun playProgression() {
		while(shouldContinue) {
			for (0 => current; current < progression.cap(); current++) {
				rgen.bar() => now;
			}
		}
	}

	fun void play() {
	}

	fun void start() {
		0 => current;
		1 => shouldContinue;
		spork ~ play() @=> shreds[0];
	}

	fun void exit() {
		0 => shouldContinue;
		for (Shred @ s : shreds) {
			if (null == s) {
				continue;
			}
			s.exit();
		}
	}
	
	fun void stop() {
		exit();
	}

	fun void silence() {
	}

	fun void unsilence() {
	}

	fun menu() {
	}
}
