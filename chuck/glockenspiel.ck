/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

public class Glockenspiel extends Instrument {
	fun Glockenspiel() {
		add(me.dir() + "glockenspiel/6_78l.wav", 78);
		add(me.dir() + "glockenspiel/79l.wav", 79);
		add(me.dir() + "glockenspiel/7_80l.wav", 80);
		add(me.dir() + "glockenspiel/81l.wav", 81);
		add(me.dir() + "glockenspiel/8_82l.wav", 82);
		add(me.dir() + "glockenspiel/83l.wav", 83);
		add(me.dir() + "glockenspiel/9_84l.wav", 84);
		add(me.dir() + "glockenspiel/85l.wav", 85);
		add(me.dir() + "glockenspiel/10_86l.wav", 86);
		add(me.dir() + "glockenspiel/87l.wav", 87);
		add(me.dir() + "glockenspiel/11_88l.wav", 88);
		add(me.dir() + "glockenspiel/89l.wav", 89);
		add(me.dir() + "glockenspiel/12_90l.wav", 90);
		add(me.dir() + "glockenspiel/91l.wav", 91);
		add(me.dir() + "glockenspiel/13_92l.wav", 92);
		add(me.dir() + "glockenspiel/93l.wav", 93);
		add(me.dir() + "glockenspiel/14_94l.wav", 94);
		add(me.dir() + "glockenspiel/95l.wav", 95);
		add(me.dir() + "glockenspiel/15_96l.wav", 96);
		add(me.dir() + "glockenspiel/97l.wav", 97);
		add(me.dir() + "glockenspiel/16_98l.wav", 98);
		add(me.dir() + "glockenspiel/99l.wav", 99);
		add(me.dir() + "glockenspiel/1_100l.wav", 100);
		add(me.dir() + "glockenspiel/101l.wav", 101);
		add(me.dir() + "glockenspiel/2_102l.wav", 102);
		add(me.dir() + "glockenspiel/103l.wav", 103);
		add(me.dir() + "glockenspiel/3_104l.wav", 104);
		add(me.dir() + "glockenspiel/105l.wav", 105);
		add(me.dir() + "glockenspiel/4_106l.wav", 106);
		add(me.dir() + "glockenspiel/107l.wav", 107);
		add(me.dir() + "glockenspiel/5_108l.wav", 108);
		add(me.dir() + "glockenspiel/109l.wav", 109);
	}
}
