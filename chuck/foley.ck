global Gain foleyVolume;
global Gain buttonVolume;

fun void playFoley(string name, Gain g, float rateNoise) {
	SndBuf buf => g;
	name => buf.read;
	1 + ((Math.randomf() - 0.5) * rateNoise) => buf.rate;
	buf.length() => now;
}

fun void listenToFoley(Event e, string name, Gain g, float rateNoise) {
	while(true) {
		e => now;
		spork ~ playFoley(name, g, rateNoise);
	}
}

private class SustainControl {
	int shouldSustain;
	Event e;

	fun SustainControl(Event ev) {
		ev @=> e;
		1 => shouldSustain;
	}

	fun void run() {
		e => now;
		1 => shouldSustain;
	}
}

fun void sustainCloud(Event e, string name, Gain g, int density, dur length) {
	Cloud cloud(name, "grain_envelopes/sin.wav", new Distribution);
	cloud.output => g;

	length => cloud.size;
	density => cloud.density;

	SustainControl c(e);
	while(true) {
		e => now;
		Shred s;
		{
			0 => c.shouldSustain;
			spork ~ c.run() @=> s;
			spork ~ cloud.play();
			length => now;
		} while(c.shouldSustain)
		s.exit();
	}
}

fun void listenToCloud(Event e, string name, Gain g, int density, dur length) {
	Cloud cloud(name, "grain_envelopes/sin.wav", new Distribution);
	cloud.output => g;

	length => cloud.size;
	density => cloud.density;

	while(true) {
		e => now;
		1 + ((Math.randomf() - 0.5)) => cloud.rate;
		spork ~ cloud.play();
	}
}

global Event f_death;
spork ~ listenToFoley(f_death, "sfx/sigh-1-female.wav", foleyVolume, 0.);

global Event f_walk;
spork ~ listenToFoley(f_walk, "sfx/footstep.wav", foleyVolume, 0.3);


Glockenspiel glockenspiel;
global Conductor conductor;
private class Button {
	fun string get() {
		return "";
	}
}

private class Button1 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][0]);
	}
}

private class Button2 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][1]);
	}
}

private class Button3 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][2]);
	}
}

private class Button4 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][3]);
	}
}

private class Button5 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][1] + 12);
	}
}

fun void listenToButton(Event e, Button b, Gain g) {
	while(true) {
		e => now;
		spork ~ playFoley(b.get(), g, 0);
	}
}


global Event button1;
spork ~ listenToButton(button1, new Button1(), buttonVolume);
global Event button2;
spork ~ listenToButton(button2, new Button2(), buttonVolume);
global Event button3;
spork ~ listenToButton(button3, new Button3(), buttonVolume);
global Event button4;
spork ~ listenToButton(button4, new Button4(), buttonVolume);
global Event button5;
spork ~ listenToButton(button5, new Button5(), buttonVolume);

while(true) {
	1::second => now;
}
