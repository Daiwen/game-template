import dn.Process;
import hxd.Key;
import tools.Action;
import haxe.ds.Option;

import ldtk.Level.NeighbourDir.*;

typedef Save = {
	var curLevelIdx : Int;
	var spawn : ldtk.Point;
}

class Game extends Process {
	public static var ME : Game;

	public var ca : dn.legacy.Controller.ControllerAccess;
	public var control : CustomControl;
	public var fx : Fx;
	public var camera : Camera;
	public var scroller : h2d.Layers;
	public var level : Level;
	public var lights : Lights;
	public var spawn : ldtk.Point;
	public var hud : ui.Hud;
	public var hero : ent.Hero;

	var mouseX : Float;
	var mouseY : Float;

	public var world : World;
	public var curLevelIdx : Int;
	public var visited : Map<Level.Coord, Bool> = new Map();
	var maxLevelIdx : Int = 0;

	var target : Option<ldtk.Point>;
	var targetRoot : h2d.Object;

	public var repl : tools.Hake;

	public function new() {
		super(Main.ME);
		ME = this;

		repl = Main.ME.repl;
		repl.addCommand("level", "Opens a menu to explore the level.", logLevel);

		control = new CustomControl();
		var showControls = function () {
			var actions = new Array();
			for (ab in tools.CustomControl.bindings.keyValueIterator()) {
				var bindings = [];
				for (b in ab.value) {
					var s = [ for (m in b.modifiers) tools.CustomControl.getKeyName(m) ];
					s.push(tools.CustomControl.getKeyName(b.key));
					bindings.push(s.join("+"));
				}
				actions.push(control.getActionName(ab.key) + ": " + bindings.join(" "));
			}
			repl.log(actions.join("\n"));
		};
		repl.addCommand("controls", "List the game controls.", showControls);

		createRootInLayers(Main.ME.root, Const.DP_BG);

		engine.backgroundColor = 0xa2a2a2;

		scroller = new h2d.Layers();
		root.add(scroller, Const.DP_BG);

		target = None;
		targetRoot = new h2d.Object();
		scroller.add(targetRoot, Const.DP_TOP);

		curLevelIdx = 0;

		var worldName =	hxd.Res.world.world.entry.getText();

		world = new World( worldName );

		fx = new Fx();
		hud = new ui.Hud();
		new systems.Kinetic();

		startLevel();

		Process.resizeAll();
	}

	public function onCdbReload() {
	}

	private function startLevel() {
		lights = new Lights();
		systems.Kinetic.ME.init();
		camera = new Camera();
		var l = world.all_worlds.Default.levels[curLevelIdx];
		level = new Level(l);

		var x = M.round(l.worldX / l.pxWid);
		var y = M.round(l.worldY / l.pxHei);
		var k = Level.Coord.Point(x, y);
		visited.set(k, true);

		level.initLevel();
		hero =
			if (spawn == null) {
				var cy = Std.int(level.hei / 2);
				new ent.Hero(2, cy);
			}
			else
				new ent.Hero(spawn.cx, spawn.cy);
		camera.trackTarget(hero, false);
	}

	public function previousLevel() {
		fx.clear();
		spawn = null;
		if (curLevelIdx > 0) {
			curLevelIdx--;
			restartLevel();
		}
	}
	
	public function updateLevel() {
		var dir = South;
		var nx = hero.cx;
		var ny = hero.cy;

		if (hero.cx == 0) {
			dir = West;
			nx = level.wid-2;
		} else if (hero.cx == level.wid-1) {
			dir = East;
			nx = 1;
		} else if (hero.cy == 0) {
			dir = North;
			ny = level.hei-2;
		} else {
			dir = South;
			ny = 1;
		}

		var iid = world.all_worlds.Default.levels[curLevelIdx].iid;
		for (n in level.level.neighbours) {
			if ( n.dir == dir ) {
				iid = n.levelIid;
				break;
			}
		}

		for (i in 0...world.all_worlds.Default.levels.length) {
			if (world.all_worlds.Default.levels[i].iid == iid) {
				curLevelIdx = i;
				break;
			}
		}

		spawn = new ldtk.Point(nx, ny);

		save();

		restartLevel();
	}

	public function nextLevel() {
		spawn = null;

		if (curLevelIdx < world.all_worlds.Default.levels.length - 1) {
			curLevelIdx++;
			if (curLevelIdx > maxLevelIdx)
				maxLevelIdx = curLevelIdx;
			restartLevel();
		}
	}

	public function restartLevel(){
		fx.clear();
		for(e in Entity.ALL)
			e.destroy();
		hero.destroy();
		level.destroy();
		lights.destroy();
		camera.destroy();
		all_gc();
		startLevel();
	}

	function e_gc() {
		if( vfx.Effect.GC==null || vfx.Effect.GC.length==0 )
			return;

		for(e in vfx.Effect.GC)
			e.dispose();
		vfx.Effect.GC = [];
	}

	function ec_gc() {
		if( ent.comp.EntityComponent.GC==null || ent.comp.EntityComponent.GC.length==0 )
			return;

		for(e in ent.comp.EntityComponent.GC)
			e.dispose();
		ent.comp.EntityComponent.GC = [];
	}


	function gc() {
		if( Entity.GC==null || Entity.GC.length==0 )
			return;

		for(e in Entity.GC)
			e.dispose();
		Entity.GC = [];
	}

	function s_gc() {
		if( systems.System.GC==null || systems.System.GC.length==0 )
			return;

		for(s in systems.System.GC)
			s.dispose();
		systems.System.GC = [];
	}

	function all_gc() {
		gc();
		ec_gc();
		e_gc();
		s_gc();
	}

	override function onDispose() {
		super.onDispose();

		fx.destroy();
		for(e in Entity.ALL)
			e.destroy();
		all_gc();
	}

	function updateMouse() {
		var gx = hxd.Window.getInstance().mouseX;
		var gy = hxd.Window.getInstance().mouseY;
		mouseX = gx/Const.SCALE - scroller.x;
		mouseY = gy/Const.SCALE - scroller.y;

		hud.invalidate();
	}

	override function preUpdate() {
		super.preUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.preUpdate();
		for(e in ent.comp.EntityComponent.ALL) if( !e.destroyed ) e.preUpdate();
		for(s in systems.System.ALL) if( !s.destroyed ) s.preUpdate();
	}

	function showTarget() {
		targetRoot.removeChildren();

		if (!Settings.assistedAiming)
			return;

		switch(target) {
			case None:
			case Some (t):
				var color = 0xffffff;
				var g = new h2d.Graphics(targetRoot);
				g.beginFill(color, 0.);
				g.lineStyle(2, color);
				g.drawRect(t.cx * Const.GRID, t.cy * Const.GRID, Const.GRID, Const.GRID);
				g.endFill();
		}
	}

	override function postUpdate() {
		super.postUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.postUpdate();
		for(e in ent.comp.EntityComponent.ALL) if( !e.destroyed ) e.postUpdate();
		for(s in systems.System.ALL) if( !s.destroyed ) s.postUpdate();
		all_gc();

		if (level.shouldChangeLevel(hero.cx, hero.cy)) {
			repl.log("Loading level");
			updateLevel();
		}

		showTarget();	

		Boot.ME.postRoot.x = scroller.x;
		Boot.ME.postRoot.y = scroller.y;
	}

	override function fixedUpdate() {
		super.fixedUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.fixedUpdate();
	}

	override function update() {
		super.update();

		updateMouse();

		for(e in Entity.ALL) if( !e.destroyed ) e.update();
		for(e in ent.comp.EntityComponent.ALL) if( !e.destroyed ) e.update();
		for(e in vfx.Effect.ALL) if( !e.destroyed ) e.update();
		for(s in systems.System.ALL) if( !s.destroyed ) s.update();

		if( control.probe(Exit) ) {
			new PauseMenu();
		}

		if(Key.isPressed(Key.B)) {
			previousLevel();
		}

		if(Key.isPressed(Key.N)) {
			nextLevel();
		}

		var aimX = getX();
		var aimY = getY();
		switch [aimX, aimY] {
			case [Some (x), Some (y)]: updateTarget(Std.int(x), Std.int(y));
			case [None, Some (y)]: updateTarget(0, Std.int(y));
			case [Some (x), None]: updateTarget(Std.int(x), 0);
			default:
		}

		if(control.probe(SelectAim)) {
			logTarget();
		}
	}

	private function getY() {
		switch [control.cprobe(AimDown), control.cprobe(AimUp)] {
			case [Some (v), _]: return Some (v);
			case [_, Some (v)]: return Some (-v);
			default: return None;
		}
	}

	private function getX() {
		switch [control.cprobe(AimRight), control.cprobe(AimLeft)] {
			case [Some (v), _]:	return Some (v);
			case [_, Some (v)]:	return Some (-v);
			default: return None;
		}
	}

	public function updateTarget(x : Int, y : Int) {
		target =
			switch (target) {
				case Some (t) :
					var ncx = t.cx + x;
					var ncy = t.cy + y;

					if (level.isValidX(ncx))
						t.cx = ncx;

					if (level.isValidY(ncy))
						t.cy = ncy;

					if (Settings.assistedAiming) {
						repl.log("Cursor at [" + t.cx + "," + t.cy + "]");

						var entities = ProtoEntity.ALL.filter(function (e) {return e.cx == t.cx && e.cy == t.cy;});

						for (e in entities) {
							repl.log(e.getLog());
						}
					}
					Some (t);
				case None: Some (new ldtk.Point(0, 0));
			}
	}

	public function getTarget() {
		return
			switch (target) {
				case Some (t) if (Settings.assistedAiming):
					new h2d.col.Point((t.cx + 0.5) * Const.GRID, (t.cy + 0.5) * Const.GRID);
				default: new h2d.col.Point(mouseX, mouseY);
			}
	}

	public function logTarget() {
		switch (target) {
			case Some (t):
				var entities = ProtoEntity.ALL.filter(function (e) {return e.cx == t.cx && e.cy == t.cy;});

				var menus = new Array();

				var back = {
					label : "Back",
					select : function () {},
				};

				menus.push(back);

				for (e in entities) {
					var o = {
						label : e.getName(),
						select : e.details,
					}

					menus.push(o);
				}

				repl.menu(menus);
			default:
		}
	}

	public function logLevel() {
		var entities = new Map();

		for (e in ProtoEntity.ALL) {
			var es = entities.get(e.getName());

			if (es == null)
				es = new Array();

			es.push(e);

			entities.set(e.getName(), es);
		}

		var menus = new Array();

		for (i in entities.keyValueIterator()) {
			var locations = [ for (e in i.value) "[" + e.cx + ", " + e.cy + "]" ];
			var o = {
				label : i.key,
				select : function () {
					repl.log(locations.join(" "));
				},
			}

			menus.push(o);
		}

		var exits = new Array();

		for (i in 0...level.wid) {
			if (!level.hasCollision(i, 0)) {
				exits.push(new ldtk.Point(i, 0));
			}

			if (!level.hasCollision(i, level.hei - 1)) {
				exits.push(new ldtk.Point(i, level.hei - 1));
			}
		}

		for (i in 0...level.hei) {
			if (!level.hasCollision(0, i)) {
				exits.push(new ldtk.Point(0, i));
			}

			if (!level.hasCollision(level.wid - 1, i)) {
				exits.push(new ldtk.Point(level.wid - 1, i));
			}
		}

		var exitLocations = [for (e in exits) "[" + e.cx + ", " + e.cy + "]"];
		var o = {
			label : "Exits",
			select : function () {
				repl.log(exitLocations.join(" "));
			},
		}
		menus.push(o);

		repl.menu(menus);
	}

	private function getCurrentState() {
		var s = {
			curLevelIdx : curLevelIdx
		  , spawn : spawn
		}

		return s;
	}

	public function save() {
		var s = getCurrentState();

		hxd.Save.save(s);
	}	

	public function load() {
		var cs = getCurrentState();
		var s : Save = hxd.Save.load(cs);

		curLevelIdx = s.curLevelIdx;
		spawn = s.spawn;

		restartLevel();
	}
}
