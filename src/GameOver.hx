import ui.Components;

@:uiComp("title-menu")
class TitleContainer extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <title-menu>
		<button public id="start"/>
		<button public id="config"/>
		<button public id="credits"/>
	</title-menu>;

	public function new(align:h2d.Flow.FlowAlign, ?parent) {
		super(parent);
		initComponent();
	}

}


class Title extends dn.Process {
	var img : h2d.Bitmap;
	var color : h3d.Vector;
	public var ca : dn.heaps.Controller.ControllerAccess;

	var center : h2d.Flow;
	var style = null;

	public function new() {
		super(Main.ME);

		ca = Main.ME.controller.createAccess("title", false);

		createRootInLayers(Main.ME.root, Const.DP_UI);
		img = new h2d.Bitmap(hxd.Res.title.toTile(), root);

		cd.setS("lock",0.2);
		color = new h3d.Vector();
		color.setColor(0xe5ae00);
		img.colorAdd = color;


		center = new h2d.Flow(root);
		center.horizontalAlign = center.verticalAlign = Middle;
		onResize();

		var root = new TitleContainer(Right, center);

		// Override
		root.start.label = "Start";
		root.config.label = "Config";
		root.credits.label = "Credits";


		root.start.onClick = function() {
			skip();
		}

		root.config.onClick = function() {
			new ConfigMenu(this);
		}

		root.credits.onClick = function() {
		}


		style = new h2d.domkit.Style();
		style.load(hxd.Res.style);
		style.addObject(root);

		dn.Process.resizeAll();
	}

	override function onDispose() {
		super.onDispose();
		ca.dispose();
	}

	var done = false;
	function skip() {
		if( done )
			return;
		color.setColor(0xffcc44);
		done = true;
	}

	override function onResize() {
		super.onResize();

		img.x = ( w()/Const.SCALE*0.5 - img.tile.width*0.5 );

		center.minWidth = center.maxWidth = w();
		center.minHeight = center.maxHeight = h();
	}

	override function postUpdate() {
		super.postUpdate();

		if( !done ) {
			color.r*=Math.pow(0.99,tmod);
			color.g*=Math.pow(0.96,tmod);
			color.b*=Math.pow(0.98,tmod);
		}
		else {
			var s = 0.1;
			color.r = M.fmax(-1, color.r - s*tmod );
			color.g = M.fmax(-1, color.g - s*tmod );
			color.b = M.fmax(-1, color.b - s*tmod );
			if( color.r<=-1 && color.g<=-1 && color.b<=-1 ) {
				Main.ME.startGame();
				destroy();
			}
		}
	}

	override function update() {
		super.update();

		style.sync();

		if( !cd.has("lock") ) {
			if( ca.isKeyboardPressed(hxd.Key.ENTER) )
					skip();
		}
	}
}
