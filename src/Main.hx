import Data;
import hxd.Key;

class MyGameFocusHelper extends dn.heaps.GameFocusHelper {
	var isFirst = true;
	override function suspendGame() {
		if (Settings.sightless)
			return;

		super.suspendGame();
	}

	override public function resumeGame() {
		super.resumeGame();

		if (!isFirst)
			return;

		isFirst = false;
		Key.initialize();
		tools.CustomControl.suspend(0.5);
		ChucK.init(continuation);
	}

	dynamic public function continuation() {
	}
}

class Main extends dn.Process {
	public static var ME : Main;

	public var repl : tools.Hake;

	public var gfh : MyGameFocusHelper;

	public function new(s:h2d.Scene, ui:h2d.Scene) {
		super();
		ME = this;

        createRoot(s);

		// Engine settings
		hxd.Timer.wantedFPS = Const.FPS;
		engine.backgroundColor = 0x000;
        #if( hl && !debug )
        engine.fullScreen = true;
        #end

		// Resources
		#if(hl && debug)
		hxd.Res.initLocal();
        #else
        hxd.Res.initEmbed();
        #end

        // Hot reloading
		#if debug
        hxd.res.Resource.LIVE_UPDATE = true;
        hxd.Res.data.watch(function() {
            delayer.cancelById("cdb");

            delayer.addS("cdb", function() {
            	Data.load( hxd.Res.data.entry.getBytes().toString() );
            	if( Game.ME!=null )
                    Game.ME.onCdbReload();
            }, 0.2);
        });
		#end

		// Assets & data init
		Assets.init();
		Settings.init();
		CustomControl.init();
		Lang.init("en");
		Data.load( hxd.Res.data.entry.getText() );

		repl = new tools.Hake(this);

		gfh = new MyGameFocusHelper(ui, Settings.uiFont);
		gfh.continuation = start;

		ui.add(gfh.root, 10);
	}

	public function start() {
		#if debug
		startGame();
		#else
		new Title();
		#end

		repl.setVisible();
	}

	public function end() {
		if( Game.ME!=null ) {
			Game.ME.destroy();
			delayer.addF( () -> new EndScreen(), 2 );
		}
	}

	public function restart() {
		if( Game.ME!=null ) {
			Game.ME.destroy();
			delayer.addF( start, 1 );
		}
	}

	public function startGame() {
		if( Game.ME!=null ) {
			Game.ME.destroy();
			delayer.addF(function() {
				new Game();
			}, 1);
		}
		else
			new Game();
	}

	public function loadGame() {
		if( Game.ME!=null ) {
			Game.ME.destroy();
			delayer.addF(function() {
				new Game();
				Game.ME.load();
			}, 1);
		}
		else {
			new Game();
			Game.ME.load();
		}
	}

	override public function onResize() {
		super.onResize();
	}

    override function update() {
	    super.update();
    }
}
