package tools;

enum Action {
	Up;
	Down;
	Left;
	Right;
	Select;
	Exit;
	Interact;
	Map;
	AimUp;
	AimDown;
	AimLeft;
	AimRight;
	SelectAim;
}
