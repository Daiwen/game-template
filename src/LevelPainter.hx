import tools.IntSet;
import tools.WFC;
import haxe.ds.Option;

@:build(tileAnnotator.JsonMacro.justTypes("./res/world/tileset_annot.json"))
class Biome {
	public var kind : TA_biome;

	public function new(k) {
		kind = k;
	}

	public function getQuarterTile(l : TA_level) {
		return { biome : kind };
	}
}

@:build(tileAnnotator.JsonMacro.justTypes("./res/world/tileset_annot.json"))
enum QTC { QT (biome : TA_biome); }

enum ConstraintLocation {
	TopLeft (c : QTC);
	Top (l : QTC, r : QTC);
	TopRight (c : QTC);
	Right (u : QTC, d : QTC);
	BottomRight (c : QTC);
	Bottom (l : QTC, r : QTC);
	BottomLeft (c : QTC);
	Left (u : QTC, d : QTC);
}

class TileDescr {
	public var c(default, null) : TA_tile;
	var level : TA_level;

	public function new (c : TA_tile, l : TA_level) {
		this.c = c;
		level = l;
	}

	static function isEmptyTile(qt) {
		return qt.biome == Empty;
	}

	static function isCompatibleEmpty(l, qt1, qt2) {
		return isEmptyTile(qt1);
	}

	static function isCompatibleQuarterTile(l, qt1, qt2) {
		return isCompatibleEmpty(l, qt1, qt2);
	}

	public static function isQuarterTileEqual(l, qt1, qt2) {
		var isEqual = (qt1, qt2) ->	qt1.biome == qt2.biome;

		return
			isEqual(qt1, qt2)
			|| isCompatibleQuarterTile(l, qt1, qt2);
	}

	public static function isTileEqual(l, c1, c2) {
		return isQuarterTileEqual(l, c1.top_left, c2.top_left) &&
			isQuarterTileEqual(l, c1.top_right, c2.top_right) &&
			isQuarterTileEqual(l, c1.bottom_left, c2.bottom_left) &&
			isQuarterTileEqual(l, c1.bottom_right, c2.bottom_right);
	}

	public function isCompatible(t : TA_tile) {
		return isTileEqual(level, c, t);
	}
}

class Neighbours extends tools.Neighbours {
	static public function all(annotations : Array<TA_annotations>) {
		var neighbours = new Array();
		for (a in annotations) {
			var c = a.constraints;
			var i = tools.Neighbours.full(annotations.length);
			var t = { neighbours : i, priority : a.priority, kind : new TileDescr(c, a.level) };
			neighbours.push(t);
		}

		return neighbours;
	}

	static public function fromAnnotations(annotations : Array<TA_annotations>) : Array<tools.Tile<TileDescr>> {
		var buckets = new Map();

		var addTile = (i, c) -> {
			var s = buckets.get(c);

			if (null == s) {
				s = new IntSet();
				buckets.set(c, s);
			}

			s.add(i);
		};

		var toEnum = (c) -> QT(c.biome);

		for (i in annotations.keyValueIterator()) {
			var constraints = i.value.constraints;
			addTile(i.key, TopLeft(toEnum(constraints.top_left)));
			addTile(i.key, TopRight(toEnum(constraints.top_right)));
			addTile(i.key, BottomRight(toEnum(constraints.bottom_right)));
			addTile(i.key, BottomLeft(toEnum(constraints.bottom_left)));

			addTile(i.key, Top(toEnum(constraints.top_left), toEnum(constraints.top_right)));
			addTile(i.key, Bottom(toEnum(constraints.bottom_left), toEnum(constraints.bottom_right)));
			addTile(i.key, Right(toEnum(constraints.top_right), toEnum(constraints.bottom_right)));
			addTile(i.key, Left(toEnum(constraints.top_left), toEnum(constraints.bottom_left)));
		}

		var getCandidates = (k) -> {
			var s = buckets.get(k);

			if (null == s) {
				s = new IntSet();
			}

			return s;
		};

		var neighbours = new Array();
		for (a in annotations) {
			var c = a.constraints;
			var i = new tools.Neighbours();

			i.topLeft = getCandidates(BottomRight(toEnum(c.top_left)));
			i.topRight = getCandidates(BottomLeft(toEnum(c.top_right)));
			i.bottomLeft = getCandidates(TopRight(toEnum(c.bottom_left)));
			i.bottomRight = getCandidates(TopLeft(toEnum(c.bottom_right)));

			i.top = getCandidates(Bottom(toEnum(c.top_left), toEnum(c.top_right)));
			i.bottom = getCandidates(Top(toEnum(c.bottom_left), toEnum(c.bottom_right)));
			i.left = getCandidates(Right(toEnum(c.top_left), toEnum(c.bottom_left)));
			i.right = getCandidates(Left(toEnum(c.top_right), toEnum(c.bottom_right)));
			var t = { neighbours : i, priority : a.priority, kind : new TileDescr(c, (a.level)) };
			neighbours.push(t);
		}

		return neighbours;
	}
}

@:build(tileAnnotator.JsonMacro.addFields("annotatedTiles", "./res/world/tileset_annot.json"))
class LevelPainter {
	var state : Array<Array<Biome>>;
	var seed : Int;
	var level : Level;

	public var bgH(default, null) : Int;
	public var bgW(default, null) : Int;

	var layerSolvers : Array<tools.WFC<TA_tile, TileDescr>>;
	var layerInvalidated : Array<Bool>;
	var bgSolver : tools.WFC<TA_tile, TileDescr>;
	var tiles : Array<Array<TA_Tile>>;
	var bgTiles : Array<TA_Tile>;
	public var topLayers(default, null) : Array<Array<TA_Tile>>;
	public var bottomLayers(default, null) : Array<Array<TA_Tile>>;
	public var bgLayer(default, null) : Array<TA_Tile>;

	final layerNames = haxe.EnumTools.createAll(TA_level);
	final topLayerNames = [ LWall ];
	final bottomLayerNames = [ LGrass, LWallEdge ];

	public static final emptyQuarterTile = {
		  biome : TA_biome.Empty
		};

	public static final emptyTile = {
		  top_left : emptyQuarterTile
	    , top_right : emptyQuarterTile
		, bottom_left : emptyQuarterTile
		, bottom_right : emptyQuarterTile
		};

	public function new(l : Level, seed : Int) {
		layerNames.reverse();
		level = l;
		state = new Array();
		this.seed = seed;

		for (cx in 0...l.wid) {
			state[cx] = new Array();

			for (cy in 0...l.hei) {
				var v = l.level.l_Biomes.getInt(cx, cy);
				var biome =
					switch (v) {
						case 1:
							level.addCollision(cx, cy);
							Wall;
						case 2: Grass;
						default: Empty;
					};

				state[cx][cy] = new Biome(biome);
			}
		}

		var defaultConstraint = emptyTile;

		layerSolvers = new Array();
		layerInvalidated = new Array();
		topLayers = new Array();
		bottomLayers = new Array();
		bgLayer = new Array();
		tiles = new Array();

		for (f in layerNames.keyValueIterator()) {
			tiles[f.key] = annotatedTiles.filter((t : TA_Tile) -> t.annotations.level == f.value || TileDescr.isTileEqual(LWallEdge, t.annotations.constraints, emptyTile));
			var annots = tiles[f.key].map((t) -> t.annotations);
			var tiles =
				switch (f.value) {
					case LWallEdge : Neighbours.all(annots);
					case (LWall | LGrass) : Neighbours.fromAnnotations(annots);
			}
			layerSolvers[f.key] = new tools.WFC(l.hei-1, l.wid-1, tiles, defaultConstraint);
			layerSolvers[f.key].seed = seed;
			layerInvalidated[f.key] = true;
		}

		initBG(seed);

		for (i in 0...(l.wid - 1)) {
			for (j in 0...(l.hei - 1)) {
				for (ls in layerSolvers.keyValueIterator()) {
					var c = {
					  top_left : state[i][j].getQuarterTile(layerNames[ls.key])
					, bottom_left : state[i][j+1].getQuarterTile(layerNames[ls.key])
					, top_right : state[i+1][j].getQuarterTile(layerNames[ls.key])
					, bottom_right : state[i+1][j+1].getQuarterTile(layerNames[ls.key])
					}

					ls.value.setTile(i, j, c);
				}
			}
		}
	}

	var grassConstraint : TA_tile;
	var grassTiles : Array<tools.Tile<TileDescr>>;

	function initBG(seed) {
		var grassQT = {
		  isWall : false
		, isAntiMagic : false
		, biome : TA_biome.Grass
		};
		var grassTile = {
		  top_left : grassQT
		, bottom_left : grassQT
		, top_right : grassQT
		, bottom_right : grassQT
		};
		grassConstraint = grassTile;

		bgTiles = annotatedTiles.filter((t : TA_Tile) -> TileDescr.isTileEqual(LWallEdge, t.annotations.constraints, grassTile));
		var annots = bgTiles.map((t) -> t.annotations);
		grassTiles = Neighbours.fromAnnotations(annots);

		resizeBG();
	}

	function resizeBG() {
		var nbgH = Math.ceil(Game.ME.stageHei/(tileSize * Const.SCALE));
		var nbgW = Math.ceil(Game.ME.stageWid/(tileSize * Const.SCALE));

		if (null != bgH && null != bgW && nbgH <= bgH && nbgW <= bgW)
			return;

		bgH = nbgH;
		bgW = nbgW;
		bgSolver = new tools.WFC(bgH, bgW, grassTiles, grassConstraint);
		bgSolver.seed = seed;
	}

	public function runWFC() {
		for (l in layerSolvers.keyValueIterator()) {
			if (!layerInvalidated[l.key])
				continue;

			layerInvalidated[l.key] = false;

			var indices = l.value.run();
			if (l.key < bottomLayerNames.length) {
				bottomLayers[l.key] = indices.map((i) -> tiles[l.key][i]);
			} else {
				topLayers[l.key-bottomLayerNames.length] = indices.map((i) -> tiles[l.key][i]);
			}
		}

		var indices = bgSolver.run();
		bgLayer = indices.map((i) -> bgTiles[i]);
	}

	function invalidateLayer(c) {
	}

	public function set(x, y, c) {
		state[x][y] = c;
		invalidateLayer(c);

		for (i in [x-1, x]) {
			for (j in [y-1, y]) {
				if (j == level.hei-1 || i == level.wid-1) {
					continue;
				}

				for (ls in layerSolvers.keyValueIterator()) {
					var c = {
					  top_left : state[i][j].getQuarterTile(layerNames[ls.key])
					, bottom_left : state[i][j+1].getQuarterTile(layerNames[ls.key])
					, top_right : state[i+1][j].getQuarterTile(layerNames[ls.key])
					, bottom_right : state[i+1][j+1].getQuarterTile(layerNames[ls.key])
					};

					ls.value.setTile(i, j, c);
				}
			}
		}
	}

	public function setSeed(s) {
		for (l in layerSolvers) {
			l.seed = s;
		}
	}

	public function resize() {
		resizeBG();
	}
}
