import dn.Process;
import hxd.Key;
import haxe.ds.Option;

import ui.Components;
import CustomControl;

typedef ModifiersOptions = {
	var control : tools.CustomControl;
	var modifiersValue : Array<tools.CustomControl.Input>;
	var redraw : () -> Void;
	var menu : tools.MenuControl;
	var process : Process;
	var backLog : () -> Void;
}

@:uiComp("modifiers-widget")
class ModifiersWidgetComp extends h2d.Flow implements h2d.domkit.Object implements tools.MenuControl.MenuItem {

	var menu : tools.MenuControl.HorizontalMenuControl;
	var parentMenu : tools.MenuControl;

	var padEvent : tools.PadEvent;

	var items : Array<tools.MenuControl.MenuItem> = new Array();

	var options : ModifiersOptions;

	private var label : String = "Modifiers";

	static var SRC = <modifiers-widget>
		<text id="modLabel" />
		<button(mTiles) id="add" />
	</modifiers-widget>

	public function addModifierFromEvent(o : ModifiersOptions, e : hxd.Event) {
		var k = AddBindingComp.getKeyCode(e);

		switch (k) {
			case None:
			case Some (k): addModifier(o, k);
		}
	}

	public function addModifier(o : ModifiersOptions, k : tools.CustomControl.Input) {

		var filtered = o.modifiersValue.filter(function (l) {return k.code == l.code && k.type == l.type;});

		if (filtered.length != 0) {
			var idx = o.modifiersValue.indexOf(filtered[0]);
			menu.select(items[idx]);
			menu.over();
			return;
		}

		o.modifiersValue.push(k);

		this.add.remove();
		menu.removeItem(this.add);

		var m : tools.MenuControl = menu;

		var options = {
			remove : function (b) {
				o.modifiersValue.remove(k);
				this.items.remove(b);
			},
			label : tools.CustomControl.getKeyName(k) + " +",
			isRemovable : true,
			menu : m,
			backLog : logSelect,
		};
		var b = new BindingItemComp(options, this);
		this.items.push(b);
		this.addChild(this.add);
		menu.addItem(b);
		menu.addItem(this.add);
		this.add.out();

		o.redraw();

		if (padEvent != null)
			padEvent.destroy();
		padEvent = null;

		show();
	}

	dynamic public function getName() {
		return label;
	}

	public function new(o : ModifiersOptions, ?parent) {
		super(parent);

		parentMenu = o.menu;
		options = o;

		var mTiles = AddBindingComp.getKeyTiles();

		initComponent();

		menu = new tools.MenuControl.HorizontalMenuControl(o.control, "Modifiers");
		menu.repl = Main.ME.repl;

		this.modLabel.font = Settings.uiFont;
		this.modLabel.text = label + ":";

		this.add.setHorizontalMargin(10);
		this.add.getName = () -> "Add modifier" ;

		var onInput = function (e : hxd.Event) {
			addModifierFromEvent(o, e);
		}

		this.add.onClick = function () {
			this.add.focus();
			padEvent = new tools.PadEvent(o.process, tools.CustomControl.gamepad);
			padEvent.onInput = addModifier.bind(o, _);
			hxd.Window.getInstance().addEventTarget(onInput);
		}

		this.add.onOver = function () {
			menu.select(this.add);
			ChucK.broadcastEvent("button2");
		}

		this.add.onOut = function () {
			hxd.Window.getInstance().removeEventTarget(onInput);
		}

		menu.addItem(this.add);


		enableInteractive = true;
		interactive.propagateEvents = true;
		interactive.onOver = function(_) {
			over();
		};

		interactive.onOut = function(_) {
			out();
		};

	}

	public function show() {
		var menus = new Array();

		var back = {
			label : "Back",
			select : options.backLog,
		};

		menus.push(back);

		for (i in items) {
			var o = {
				label : i.getName(),
				select : i.logSelect,
			}

			menus.push(o);
		}

		var add = {
			label : "Add modifier",
			select : this.add.logSelect,
		};

		menus.push(add);

		Main.ME.repl.menu(menus);
	}

	public function click() {
		menu.click();
	}

	public function logSelect() {
		show();
	}

	public function over() {
		menu.over();
		parentMenu.select(this);
	}

	public function out() {
		menu.out();
	}

	public function unroll() {
		menu.next();
	}

	public function register(m : tools.MenuControl.Menu) {
	}

	public function rollBack() {
		menu.back();
	}
}

typedef AddBindingOptions = {
	control: tools.MenuControl,
	action : tools.Action,
	addBinding : (tools.CustomControl.KeyBinding) -> Void,
	process : Process,
}

@:uiComp("add-binding")
class AddBindingComp extends h2d.Object implements h2d.domkit.Object {
	var keyValue : Null<tools.CustomControl.Input> = null;
	var modifiersValue : Array<tools.CustomControl.Input> = new Array();
	var action : tools.Action;

	var parentControl : tools.MenuControl;

	var menu : tools.MenuControl.VerticalMenuControl;

	var padEvent : tools.PadEvent;

	static var SRC = <add-binding>
		<anchor id="background" />
		<container id="widgets">
			<modifiers-widget(mOptions) id="modifiers" />
			<container id="key">
				<text id="keyLabel" />
				<button(kTiles) id="aKey" />
			</container>
			<container id="buttons">
				<button(abTiles) id="addBinding" />
				<button(eTiles) id="exit" />
			</container>
		</container>
	</add-binding>;

	public static function getKeyCode(e : hxd.Event) {
		return switch (e.kind) {
			case EKeyDown : Some (tools.CustomControl.key(e.keyCode));
			case EPush : Some (tools.CustomControl.key(e.button));
			case EWheel | EFocus | EFocusLost | EKeyUp | EReleaseOutside
			| ETextInput | ERelease | EMove | EOver | EOut | ECheck : None;
		};
	}

	public static function getKeyTiles() {
		var w = 32;
		var h = 32;
		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xaf7f66, w, h)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xaf7f66, w, h)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xa85959, w, h)));
		return {
			width : w,
			height : h,
			active : activeTile,
			hover : hoverTile,
			base : baseTile,
		};
	}

	private function getAddTiles() {
		var w = 32;
		var h = 32;
		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.PlusButton)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.PlusButton)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxHover)));
		return {
			width : w,
			height : h,
			active : activeTile,
			hover : hoverTile,
			base : baseTile,
		};
	}

	private function getExitTiles() {
		var w = 32;
		var h = 32;
		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxTicked)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxTicked)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxHover)));
		return {
			width : w,
			height : h,
			active : activeTile,
			hover : hoverTile,
			base : baseTile,
		};
	}

	function addKeyBindingFromEvent(e : hxd.Event) {
		var k = getKeyCode(e);

		switch (k) {
			case None:
			case Some (k) : addKeyBinding(k);
		}
	}

	function addKeyBinding(k : tools.CustomControl.Input) {
		this.aKey.label = tools.CustomControl.getKeyName(k);
		this.aKey.blur();

		keyValue = k;

		redraw();

		hxd.Window.getInstance().removeEventTarget(addKeyBindingFromEvent);

		if (padEvent != null)
			padEvent.destroy();
		padEvent = null;

		show();
	}

	public function new(o : AddBindingOptions, ?parent) {
		super(parent);

		parentControl = o.control;
		menu = new tools.MenuControl.VerticalMenuControl(o.control.control, "Add binding");
		menu.repl = Main.ME.repl;

		this.action = o.action;

		var eTiles = getExitTiles();
		var abTiles = getAddTiles();
		var kTiles = getKeyTiles();

		var m : tools.MenuControl = menu;

		var mOptions = {
			control : menu.control,
			modifiersValue : modifiersValue,
			redraw : redraw,
			menu : m,
			process : o.process,
			backLog : show,
		};

		initComponent();

		this.keyLabel.font = Settings.uiFont;
		this.keyLabel.text = "Key:";

		this.widgets.layout = Vertical;
		this.modifiers.layout = Horizontal;
		this.modifiers.verticalAlign = Top;
		this.key.layout = Horizontal;
		this.key.verticalAlign = Top;
		this.buttons.layout = Horizontal;
		this.buttons.horizontalAlign = Middle;

		this.widgets.padding = 5;

		this.exit.onOver = function () {
			menu.select(this.exit);
			ChucK.broadcastEvent("button2");
		}
		this.exit.onClick = function () {
			this.remove();
			menu.rollBack();
			parentControl.logSelect();
		};

		this.addBinding.onOver = function () {
			menu.select(this.addBinding);
			ChucK.broadcastEvent("button2");
		}
		this.addBinding.onClick = function () {
			if (keyValue != null) {
				var b = { key : keyValue, modifiers : modifiersValue,  protected : false };
				tools.CustomControl.addBinding(action, b);
				o.addBinding(b);
			}

			this.remove();
			menu.rollBack();
			parentControl.logSelect();
		};

		this.aKey.setHorizontalMargin(10);
		this.aKey.onClick = function () {
			padEvent = new tools.PadEvent(o.process, tools.CustomControl.gamepad);
			padEvent.onInput = addKeyBinding;
			hxd.Window.getInstance().addEventTarget(addKeyBindingFromEvent);
		}
		this.aKey.onOver = function () {
			menu.select(this.aKey);
			ChucK.broadcastEvent("button2");
		}

		redraw();

		menu.addItem(this.modifiers);
		menu.addItem(this.aKey);
		menu.addItem(this.addBinding);
		menu.addItem(this.exit);
	}

	public function redraw() {
		this.background.removeChildren();

		var bg = new h2d.Graphics(this.background);
		bg.beginFill(0xbdb781, 1.);
		bg.drawRoundedRect(0, 0, this.widgets.outerWidth, this.widgets.outerHeight, 10);
		bg.endFill();
	}

	private function show() {
		var back = {
			label : "Back",
			select : this.exit.logSelect
		};
		var modifiers = {
			label : "Modifiers",
			select : this.modifiers.logSelect,
		};

		var key = {
			label : keyValue == null ? "Add key" : "Replace " + tools.CustomControl.getKeyName(keyValue),
			select : this.aKey.logSelect
		};
		var add = {
			label : "Add",
			select : this.addBinding.logSelect
		}

		Main.ME.repl.menu([ back, modifiers, key, add ]);
	}

	public function unroll() {
		menu.unroll();
		show();
	}
}

typedef BindingOptions = {
	var remove : (tools.MenuControl.MenuItem) -> Void;
	var label : String;
	var isRemovable : Bool;
	var menu : tools.MenuControl;
	var backLog : () -> Void;
}

class BindingItemComp extends h2d.Flow implements tools.MenuControl.MenuItem {

	var button : ButtonComp;

	var label : h2d.Text;

	var menu : tools.MenuControl;
	var options : BindingOptions;

	dynamic public function getName() {
		return "Remove " + name;
	}

	private function getTiles() {
		var w = 32;
		var h = 32;
		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxTicked)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxTicked)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxHover)));
		return {
			width : w,
			height : h,
			active : activeTile,
			hover : hoverTile,
			base : baseTile,
		};
	}

	public function new(o:BindingOptions, ?parent) {
		super(parent);

		this.label = new h2d.Text(Settings.uiFont, this);
		this.label.text = o.label;
		this.menu = o.menu;

		name = o.label;
		options = o;

		var tiles = getTiles();
		this.button = new ButtonComp(tiles);

		if (o.isRemovable) {
			onOver = function () {
				menu.select(this);
				this.addChild(this.button);
				this.interactive.focus();
				backgroundTile = h2d.Tile.fromColor(0x222222, 1, 1);
				ChucK.broadcastEvent("button2");
			}

			onOut = function () {
				this.button.remove();
				backgroundTile = null;
			}

			onClick = function () {
				o.remove(this);
				this.remove();
			}
			this.button.onPush = onClick;
			this.button.propagateEvents = true;
		}

		enableInteractive = true;
		interactive.propagateEvents = true;
		interactive.onOver = function(_) {
			onOver();
		};

		if (o.isRemovable) {
			interactive.onFocusLost = function(_) {
				onOut();
			};
		} else {
			interactive.onOut = function(_) {
				onOut();
			};
		}

		this.paddingLeft = 10;
		this.paddingRight = 10;
		this.minHeight = 32;
		this.verticalAlign = Middle;
	}

	private dynamic function onOver() {
		menu.select(this);
		backgroundTile = h2d.Tile.fromColor(0x222222, 1, 1);
		ChucK.broadcastEvent("button2");
	}

	private dynamic function onOut() {
		backgroundTile = null;
	}

	private dynamic function onClick() {
	}

	public function click() {
		onClick();
	}

	public function over() {
		onOver();
	}

	public function out() {
		onOut();
	}

	public function logSelect() {
		click();
		options.backLog();
	}

	public function unroll() {
	}

	public function register(m : tools.MenuControl.Menu) {
	}

	public function rollBack() {
	}
}

typedef ActionOptions = {
	var action : tools.Action;
	var bindings : Array<tools.CustomControl.KeyBinding>;
	var open : (tools.Action, (tools.CustomControl.KeyBinding) -> Void) -> h2d.Object;
	var control : tools.CustomControl;
	var menu : tools.MenuControl;
	var backLog : () -> Void;
}

@:uiComp("action-item")
class ActionItemComp extends h2d.Flow implements h2d.domkit.Object implements tools.MenuControl.MenuItem {
	public var label : h2d.Flow;

	var menu : tools.MenuControl.HorizontalMenuControl;
	var parentMenu : tools.MenuControl;

	var addButton : ButtonComp;
	var options : ActionOptions;

	static var SRC = <action-item>
		<container id="wrapper">
		</container>
	</action-item>;

	private function getTiles() {
		var w = 32;
		var h = 32;
		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.PlusButton)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.PlusButton)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxHover)));
		return {
			width : w,
			height : h,
			active : activeTile,
			hover : hoverTile,
			base : baseTile,
		};
	}

	private function getLabelBinding(b : tools.CustomControl.KeyBinding) {
		var s = "";

		for (k in b.modifiers) {
			s += tools.CustomControl.getKeyName(k);
			s += " + ";
		}

		return (s + tools.CustomControl.getKeyName(b.key));
	}

	private function addBinding (a, b) {
		addButton.remove();
		menu.removeItem(addButton);

		var m : tools.MenuControl = menu;

		var options = {
			remove : function (i) {
				menu.removeItem(i);
				tools.CustomControl.removeBinding(a, b); },
			label : getLabelBinding(b),
			isRemovable : true,
			menu : m,
			backLog : logSelect,
		};
		var b = new BindingItemComp(options, this.wrapper);

		menu.addItem(b);
		menu.addItem(addButton);
		this.wrapper.addChild(addButton);
		addButton.out();
	}

	dynamic public function getName() {
		return name;
	}

	public function new(o:ActionOptions, ?parent) {
		super(parent);
		initComponent();

		parentMenu = o.menu;
		options = o;

		label = new h2d.Flow();

		menu = new tools.MenuControl.HorizontalMenuControl(o.control, name);
		menu.repl = Main.ME.repl;

		var txt = new h2d.Text(Settings.uiFont, label);
		txt.text = getLabel(o.action);

		name = txt.text;

		verticalAlign = Middle;

		this.wrapper.minHeight = 50;
		this.wrapper.paddingLeft = 30;
		this.wrapper.verticalAlign = Middle;
		this.label.verticalAlign = Middle;
		this.label.minHeight = 50;

		var m : tools.MenuControl = menu;

		for (b in o.bindings) {
			var options = {
				remove : function (i) {
					menu.removeItem(i);
					tools.CustomControl.removeBinding(o.action, b); },
				label : getLabelBinding(b),
				isRemovable : !b.protected,
				menu : m,
				backLog : logSelect,
			};
			var b = new BindingItemComp(options, this.wrapper);

			menu.addItem(b);
		}

		addButton = new ButtonComp(getTiles(), this.wrapper);
		addButton.onClick = function () {
			var b = o.open(o.action, addBinding.bind(o.action, _));
			var p = addButton.localToGlobal();
			b.x = p.x;
			b.y = p.y;
		};
		addButton.onOver = function () {
			menu.select(addButton);
			ChucK.broadcastEvent("button3");
		}
		addButton.getName = () -> "Add binding";

		menu.addItem(addButton);

		enableInteractive = true;
		interactive.propagateEvents = true;
		interactive.onOver = function(_) {
			over();
		};

		interactive.onOut = function(_) {
			out();
		};
	}

	public function click() {
		Main.ME.repl.log(getName());
		menu.click();
	}

	public function over() {
		menu.over();
		label.backgroundTile = h2d.Tile.fromColor(0x222222, 1, 1);
		parentMenu.select(this);
	}

	public function out() {
		menu.out();
		label.backgroundTile = null;
	}

	public function logSelect() {
		var item = {
			label : "Back",
			select : options.backLog,
		};
		menu.show([item]);
	}

	public function unroll() {
		menu.next();
	}

	public function register(m : tools.MenuControl.Menu) {
	}

	public function rollBack() {
		menu.back();
	}

	private function getLabel(a : tools.Action) {
		return menu.control.getActionName(a);
	}
}

typedef CheckboxOptions = {
	var onClick : () -> Void;
	var init : Bool;
	var label : String;
	var backLog : () -> Void;
}

@:uiComp("checkbox-item")
class CheckboxItemComp extends h2d.Flow implements h2d.domkit.Object implements tools.MenuControl.MenuItem {
	public var label : h2d.Flow;

	private var dName : String;

	private var options : CheckboxOptions;

	static var SRC = <checkbox-item>
		<container id="wrapper">
			<checkbox(tiles) id="box" />
		</container>
	</checkbox-item>;

	dynamic public function getName() {
		return dName + ": " + (isChecked() ? "On" : "Off");
	}

	private function getTiles() {
		var w = 32;
		var h = 32;
		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxUnticked)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxTicked)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.CheckboxHover)));
		return {
			width : w,
			height : h,
			active : activeTile,
			hover : hoverTile,
			base : baseTile,
		};
	}

	public function new(o:CheckboxOptions, ?parent) {
		super(parent);
		var tiles = getTiles();
		initComponent();

		label = new h2d.Flow();
		var txt = new h2d.Text(Settings.uiFont, label);
		txt.text = o.label;

		dName = o.label;

		this.box.onClick = o.onClick;
		this.box.onOver = function () {
			ChucK.broadcastEvent("button2");
		}
		setIsChecked(o.init);

		verticalAlign = Middle;

		this.wrapper.minHeight = 50;
		this.wrapper.paddingLeft = 30;
		this.label.minHeight = 50;

		options = o;
	}

	private function show() {
		Main.ME.repl.log(getName());
	}

	public function logSelect() {
		click();
		show();
		options.backLog();
	}

	public function click() {
		this.box.click();
	}

	public function over() {
		this.box.over();
	}

	public function out() {
		this.box.out();
	}

	public function unroll() {
	}

	public function register(m : tools.MenuControl.Menu) {
	}

	public function rollBack() {
	}

	public function setIsChecked(b : Bool) {
		this.box.setIsChecked(b);
	}

	public function isChecked() {
		return this.box.isChecked;
	}
}

typedef SliderOptions = {
       var getCall: (Float -> Void) -> Void;
       var setCall : (Float) -> Void;
	   var backLog : () -> Void;
}

@:uiComp("slider-item")
class SliderItemComp extends h2d.Flow implements h2d.domkit.Object implements tools.MenuControl.MenuItem {

	public var label : h2d.Flow;
	var slider : h2d.Slider;

	var options : SliderOptions;

	private var dName : String;

	static var SRC = <slider-item>
		<container id="wrapper" />
		</slider-item>;

	dynamic public function getName() {
		return dName + ": " + Std.int(slider.value * 100) + "%";
	}

	public function new(o:SliderOptions, label:String, ?parent) {
		super(parent);

		initComponent();


		this.label = new h2d.Flow();
		var txt = new h2d.Text(Settings.uiFont, this.label);
		txt.text = label;

		dName = label;

		this.wrapper.minHeight = 50;
		this.wrapper.paddingLeft = 30;
		this.label.minHeight = 50;

		slider = new h2d.Slider(64, 16, this.wrapper);
		o.getCall(function (v) { slider.value = v; });
		slider.onChange = function () {
			o.setCall(slider.value);
		}
		slider.tile = Assets.buttons.getTile(Assets.buttonsDict.SliderBar);
		slider.cursorTile = Assets.buttons.getTile(Assets.buttonsDict.SliderCursor);
		slider.onOver = function(_) { over(); };

		options = o;
	}

	private function show() {
		Main.ME.repl.log(getName());
	}

	public function logSelect() {
		show();

		var back = {
			label : "Back",
			select : options.backLog
		};
		var up = {
			label : "Increase",
			select : function () {
				increase();
				logSelect();
			}
		};
		var down = {
			label : "Decrease",
			select : function () {
				decrease();
				logSelect();
			}
		};

		Main.ME.repl.menu([ back, up, down ]);
	}

	public function click() {
	}

	public function over() {
			ChucK.broadcastEvent("button2");
	}

	public function out() {
	}

	public function rollBack() {
		decrease();
	}

	public function unroll() {
		increase();
	}

	public function register(m : tools.MenuControl.Menu) {
	}

	private function increase() {
		slider.value += 0.05;
		options.setCall(slider.value);
	}

	private function decrease() {
		slider.value -= 0.05;
		options.setCall(slider.value);
	}
}

typedef ButtonsOptions = {
	var control : tools.CustomControl;
	var showSettings : () -> Void;
	var showControl : () -> Void;
	var exit : () -> Void;
	var menu : tools.MenuControl;
}

@:uiComp("button-menu")
class ButtonMenuComp extends h2d.Flow implements h2d.domkit.Object implements tools.MenuControl.MenuItem {
	var menu : tools.MenuControl.HorizontalMenuControl;

	var parentMenu : tools.MenuControl;

	static var SRC = <button-menu>
			<button(sTiles) id="settingsMode" />
			<button(cTiles) id="controlMode" />
			<button(eTiles) id="exit" />
		</button-menu>

	dynamic public function getName() {
		return "Back";
	}

	public function new(o:ButtonsOptions, ?parent) {
		super(parent);

		var sTiles = Title.getTiles();
		var cTiles = Title.getTiles();
		var eTiles = Title.getTiles();

		parentMenu = o.menu;

		initComponent();

		menu = new tools.MenuControl.HorizontalMenuControl(o.control, "Back");
		menu.repl = Main.ME.repl;

		var onUnroll = function () {
			switch (menu.parent) {
				case None:
				case Some (p) :
					p.next();
					rollBack();
			}
		}

		this.settingsMode.label = "Settings";
		this.settingsMode.onClick = this.settingsMode.onOver = function () {
			o.showSettings();
			menu.select(this.settingsMode);
			parentMenu.select(this);
			ChucK.broadcastEvent("button1");
		}
		this.settingsMode.onUnroll = onUnroll;

		this.controlMode.label = "Custom Control";
		this.controlMode.onClick = this.controlMode.onOver = function () {
			o.showControl();
			menu.select(this.controlMode);
			parentMenu.select(this);
			ChucK.broadcastEvent("button1");
		}
		this.controlMode.onUnroll = onUnroll;

		this.exit.label = "Exit";
		this.exit.onClick = o.exit;
		this.exit.onOver = function () {
			menu.select(this.exit);
			parentMenu.select(this);
			ChucK.broadcastEvent("button1");
		}

		this.settingsMode.setHorizontalMargin(10);
		this.controlMode.setHorizontalMargin(10);
		this.exit.setHorizontalMargin(10);

		menu.addItem(this.settingsMode);
		menu.addItem(this.controlMode);
		menu.addItem(this.exit);

		menu.logSelect();

		enableInteractive = true;
		interactive.propagateEvents = true;
		interactive.onOver = function(_) {
			over();
		};
		interactive.onOut = function(_) {
			out();
		};
	}

	public function logSelect() {
		click();
		menu.logSelect();
	}

	public function click() {
		menu.click();
	}

	public function over() {
		menu.over();
	}

	public function out() {
		menu.out();
	}

	public function unroll() {
		menu.next();
	}

	public function register(m : tools.MenuControl.Menu) {
	}

	public function rollBack() {
		menu.back();
	}
}

typedef SettingsOptions = {
	var settingsControl : tools.MenuControl;
	var controlControl : tools.MenuControl;
	var close : () -> Void;
	var open : (tools.Action, (tools.CustomControl.KeyBinding)->Void) -> h2d.Object;
	var width : Int;
	var height : Int;
}

@:uiComp("settings-menu")
class SettingsMenuContainer extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <settings-menu>
		<button-menu(buttonOption) id="buttons" />
		<container id="settings">
			<container id="labels" />
			<container id="widgets">
				<slider-item(masterOption, "Master Volume") id="volume" />
				<slider-item(musicOption, "Music Volume") id="musicVolume" />
				<slider-item(soundEffectsOption, "Sound effects volume") id="soundEffect" />
				<checkbox-item(particlesOptions) id="particles" />
				<slider-item(shakePowerOption, "Shake power") id="shakePower" />
				<checkbox-item(shakesOptions) id="shakes" />
				<checkbox-item(shadersOptions) id="shaders" />
				<slider-item(speedOption, "Speed") id="speed" />
				<slider-item(inputSuspendOption, "Input suspend") id="suspend" />
				<checkbox-item(assistedAimingOptions) id="assistedAiming" />
				<checkbox-item(fontSizeOptions) id="font" />
				<checkbox-item(textHintsOptions) id="hints" />
				<checkbox-item(discreteOptions) id="discrete" />
				<checkbox-item(sightlessOptions) id="sightless" />
			</container>
		</container>
		<container id="control">
			<container id="actions" />
			<container id="keys" />
		</container>
	</settings-menu>;

	public function new(o : SettingsOptions, ?parent) {
		super(parent);

		var bControl = function () {
			o.controlControl.logSelect();
			this.settings.visible = false;
			this.control.visible = true;
		};

		var sControl = function () {
			o.settingsControl.logSelect();
			this.settings.visible = true;
			this.control.visible = false;
		};

		var m : tools.MenuControl = o.controlControl;

		var buttonOption = {
			control : o.settingsControl.control,
			showSettings : sControl,
			showControl : bControl,
			exit : o.close,
			menu : m,
		};

		var masterOption = { getCall : Assets.getVolume, setCall : function(v:Float) { return Assets.setVolume(v); }, backLog : o.settingsControl.logSelect };
		var musicOption = { getCall : Assets.getMusicVolume, setCall : function(v:Float) { return Assets.setMusicVolume(v); }, backLog : o.settingsControl.logSelect };
		var soundEffectsOption = { getCall : Assets.getSoundEffectsVolume, setCall : function(v:Float) { return Assets.setSoundEffectsVolume(v); }, backLog : o.settingsControl.logSelect };
		var shakePowerOption = { getCall : function(f) { f(Settings.shakePower); }, setCall : function(v:Float) { Settings.shakePower = v; }, backLog : o.settingsControl.logSelect };
		var speedOption = { getCall : function(f) { f(Settings.speed); }, setCall : function(v:Float) { Settings.speed = v; }, backLog : o.settingsControl.logSelect };
		var inputSuspendOption = { getCall : function(f) { f(Settings.inputSuspend/3); }, setCall : function(v:Float) { Settings.inputSuspend = v*3; }, backLog : o.settingsControl.logSelect };

		var particlesOptions = {
			onClick :
				function () {
					Settings.particles = !Settings.particles;
					this.particles.setIsChecked(Settings.particles);
				},
			init : Settings.particles,
			label : "Show particles",
			backLog : o.settingsControl.logSelect
		}

		var shakesOptions = {
			onClick :
				function () {
					Settings.shakes = !Settings.shakes;
					this.shakes.setIsChecked(Settings.shakes);
				},
			init : Settings.shakes,
			label : "Show shakes",
			backLog : o.settingsControl.logSelect
		}

		var shadersOptions = {
			onClick :
				function () {
					Settings.animatedEffects = !Settings.animatedEffects;
					this.shaders.setIsChecked(Settings.animatedEffects);
				},
			init : Settings.animatedEffects,
			label : "Show animated effects",
			backLog : o.settingsControl.logSelect
		}

		var assistedAimingOptions = {
			onClick :
				function () {
					Settings.assistedAiming = !Settings.assistedAiming;
					this.assistedAiming.setIsChecked(Settings.assistedAiming);
				},
			init : Settings.assistedAiming,
			label : "Assisted aiming",
			backLog : o.settingsControl.logSelect
		}

		var fontSizeOptions = {
			onClick :
				function () {
					if (this.font.isChecked()) {
						Settings.gameFont = Assets.fontPixel;
						Settings.uiFont = Assets.fontMedium;
					} else {
						Settings.gameFont = Assets.fontSmall;
						Settings.uiFont = Assets.fontLarge;
					}

					this.font.setIsChecked(!this.font.isChecked());
				},
			init : Settings.uiFont == Assets.fontLarge,
			label : "Large font",
			backLog : o.settingsControl.logSelect
		}

		var textHintsOptions = {
			onClick :
				function () {
					Settings.textHints = !Settings.textHints;
					this.hints.setIsChecked(Settings.textHints);
				},
			init : Settings.textHints,
			label : "Text hints",
			backLog : o.settingsControl.logSelect
		}

		var discreteOptions = {
			onClick :
				function () {
					Settings.discrete = !Settings.discrete;
					this.discrete.setIsChecked(Settings.discrete);
				},
			init : Settings.discrete,
			label : "Discrete motion",
			backLog : o.settingsControl.logSelect
		}


		var sightlessOptions = {
			onClick :
				function () {
					Settings.sightless = !Settings.sightless;
					this.sightless.setIsChecked(Settings.sightless);
					Main.ME.repl.setVisible();
					if (Settings.sightless) {
						Settings.assistedAiming = true;
						this.assistedAiming.setIsChecked(Settings.assistedAiming);
						Settings.discrete = true;
						this.discrete.setIsChecked(Settings.discrete);
						o.settingsControl.logSelect();
					}
				},
			init : Settings.sightless,
			label : "Sightless mode",
			backLog : o.settingsControl.logSelect
		}

		initComponent();

		this.layout = Vertical;
		this.horizontalAlign = Middle;
		this.buttons.layout = Horizontal;
		this.buttons.horizontalAlign = Middle;
		this.buttons.verticalAlign = Middle;

		this.settings.layout = Horizontal;
		this.settings.verticalAlign = Top;

		this.labels.layout = Vertical;
		this.widgets.layout = Vertical;
		this.labels.verticalAlign = Top;
		this.widgets.verticalAlign = Top;

		this.control.layout = Horizontal;
		this.control.verticalAlign = Top;

		this.actions.layout = Vertical;
		this.actions.verticalAlign = Top;
		this.keys.layout = Vertical;
		this.keys.verticalAlign = Top;

		this.control.visible = false;


		initSettings(o.settingsControl);

		initControl(o.controlControl, o.open);

		this.settings.overflow = Scroll;
		this.control.overflow = Scroll;

		resize(o.width, o.height);

		var onNext = function(o : h2d.Object, i) {
			if ( i > Std.int(o.numChildren / 2) ) {
				this.control.scrollPosY += 50;
				this.control.reflow();
			}
		}

		var onBack = function(o : h2d.Object, i) {
			if ( i < Std.int(o.numChildren / 2) ) {
				this.control.scrollPosY -= 50;
				this.control.reflow();
			}
		}

		o.settingsControl.onNext = onNext.bind(this.labels, _);
		o.settingsControl.onBack = onBack.bind(this.labels, _);

		o.controlControl.onNext = onNext.bind(this.actions, _);
		o.controlControl.onBack = onBack.bind(this.actions, _);
	}

	function initSettings(c : tools.MenuControl) {
		this.labels.addChild(this.volume.label);
		this.labels.addChild(this.musicVolume.label);
		this.labels.addChild(this.soundEffect.label);
		this.labels.addChild(this.particles.label);
		this.labels.addChild(this.shakePower.label);
		this.labels.addChild(this.shakes.label);
		this.labels.addChild(this.shaders.label);
		this.labels.addChild(this.speed.label);
		this.labels.addChild(this.suspend.label);
		this.labels.addChild(this.assistedAiming.label);
		this.labels.addChild(this.font.label);
		this.labels.addChild(this.hints.label);
		this.labels.addChild(this.discrete.label);
		this.labels.addChild(this.sightless.label);

		c.addItem(this.buttons);
		c.addItem(this.volume);
		c.addItem(this.musicVolume);
		c.addItem(this.soundEffect);
		c.addItem(this.particles);
		c.addItem(this.shakePower);
		c.addItem(this.shakes);
		c.addItem(this.shaders);
		c.addItem(this.speed);
		c.addItem(this.suspend);
		c.addItem(this.assistedAiming);
		c.addItem(this.font);
		c.addItem(this.hints);
		c.addItem(this.discrete);
		c.addItem(this.sightless);
	}

	function initControl(c : tools.MenuControl, open : (tools.Action, (tools.CustomControl.KeyBinding)->Void) -> h2d.Object) {
		c.addItem(this.buttons);

		var m : tools.MenuControl = c;

		for (kv in tools.CustomControl.bindings.keyValueIterator()) {

			var o = {
				action : kv.key,
				bindings : kv.value,
				open : open,
				control : c.control,
				menu : m,
				backLog : c.logSelect
			};

			var a = new ActionItemComp(o, this.keys);
			this.actions.addChild(a.label);
			c.addItem(a);
		}
	}

	public function isControl() {
		return this.control.visible;
	}

	public function resize(w, h) {
		this.settings.maxHeight = Std.int(h*0.75);
		this.control.maxHeight = Std.int(h*0.75);
	}
}


class SettingsMenu extends dn.Process {
	var control : CustomControl;
	var mask : h2d.Bitmap;

	var back : dn.Process;

	var center : h2d.Flow;

	var settingsControl : tools.MenuControl;
	var controlControl : tools.MenuControl;

	var settings : SettingsMenuContainer;

	public function new(back: dn.Process) {
		super(Main.ME);

		this.back = back;

		createRoot(Boot.ME.uiRoot);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering
		var tf = new h2d.Text(Assets.fontLarge, root);
		tf.text = "Config";

		control = new CustomControl();

		back.pause();

		mask = new h2d.Bitmap(hxd.Res.title.toTile(), root);
		root.under(mask);

		createChildProcess(
				function(c) {
					// Resize dynamically
					tf.setScale( M.imax(1, Math.floor( stageWid*0.05 / tf.textWidth )) );
					tf.x = Std.int( stageWid*0.5 - tf.textWidth*tf.scaleX*0.5 );
					tf.y = Std.int( stageHei*0.1 - tf.textHeight*tf.scaleY*0.5 );

				}, true
				);

		center = new h2d.Flow(root);
		center.horizontalAlign = Middle;
		center.verticalAlign = Top;
		onResize();

		settingsControl = new tools.MenuControl.VerticalMenuControl(control, "Settings");
		settingsControl.repl = Main.ME.repl;
		controlControl = new tools.MenuControl.VerticalMenuControl(control, "Control");
		controlControl.repl = Main.ME.repl;

		var p : dn.Process = this;

		var open = function (a, addBinding) {
			var options = {
				control : controlControl,
				action : a,
				addBinding : addBinding,
				process : p,
			};
			var c = new AddBindingComp(options);
			var o : h2d.Object = c;
			root.add(o, 2);
			c.unroll();
			return o;
		}

		var options = {
			settingsControl : settingsControl,
			controlControl : controlControl,
			close : close,
			open : open,
			width : stageWid,
			height : stageHei
		};
		settings = new SettingsMenuContainer(options, center);

		dn.Process.resizeAll();
	}

	override function onResize() {
		super.onResize();

		mask.scaleX = M.ceil(stageWid);
		mask.scaleY = M.ceil(stageHei);

		center.y = Std.int( stageHei*0.15);

		center.minWidth = center.maxWidth = stageWid;
		center.minHeight = center.maxHeight = Std.int(stageHei*0.85);

		if (settings != null) {
			settings.resize(stageWid, stageHei);
		}
	}

	override function onDispose() {
		super.onDispose();
		back.resume();
	}


	public function close() {
		if( !destroyed ) {
			destroy();
		}
	}

	override function update() {
		super.update();

		if (control.probe(Exit)) {
			close();
		}

		if (settings != null) {
			if (settings.isControl()) {
				controlControl.update();
			} else {
				settingsControl.update();
			}
		}
	}
}
