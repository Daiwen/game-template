package ent.comp;

enum Kind {
	Static;
	Attached;
}

class Displayable extends EntityComponent {
	var display : Null<h2d.Object>;
	var kind : Kind;

	var cnt = 0;

	public function new (e : Entity) {
		super(e);
	}

	override function dispose() {
		super.dispose();

		if (display != null)
			display.remove();
	}

	override function update() {
		super.update();

		if (display != null && kind == Attached) {
			display.x = me.headX;
			display.y = me.headY;
		}
	}

	public function show(s, ?k = Attached) {
		clearDisplay();
		display = new h2d.Object();
		game.scroller.add(display, Const.DP_UI);
		var tf = new h2d.Text(Settings.gameFont, display);
		tf.text = s;
		tf.x = -Std.int( tf.textWidth );
		tf.setScale(2);

		display.x = me.centerX;
		display.y = me.centerY;
	}

	public function showForS(msg, s, ?k = Attached) {
		show(msg, k);
		cd.setS("" + cnt, s, true, clearDisplay);
		cnt++;
	}

	private function clearDisplay() {
		if (display != null)
			display.remove();

		cd.unset("" + (cnt-1));
	}
}
