import tools.Action;
import hxd.Key;

class CustomControl extends tools.CustomControl {
	public function getActionName(a : Action) {
		return
			switch a {
				case Up : "Up";
				case Down : "Down";
				case Left : "Left";
				case Right : "Right";
				case Select : "Select";
				case Exit : "Exit";
				case Interact : "Interact";
				case Map : "Map";
				case AimUp : "Aim Up";
				case AimDown : "Aim Down";
				case AimLeft : "Aim Left";
				case AimRight : "Aim Right";
				case SelectAim : "Select aim";
			}
	}

	public function getSuspendDuration() {
		return Settings.inputSuspend;
	}

	static public function init() {
		tools.CustomControl.init();
		tools.CustomControl.addBinding(Interact, { modifiers : new Array(), key : tools.CustomControl.key(Key.E), protected : false });

		tools.CustomControl.addBinding(Map, { modifiers : new Array(), key : tools.CustomControl.key(Key.M), protected : false });

		tools.CustomControl.addBinding(AimUp, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.Z), protected : false });
		tools.CustomControl.addBinding(AimUp, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.W), protected : false });
		tools.CustomControl.addBinding(AimUp, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.UP), protected : false });

		tools.CustomControl.addBinding(AimDown, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.S), protected : false });
		tools.CustomControl.addBinding(AimDown, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.DOWN), protected : false });

		tools.CustomControl.addBinding(AimLeft, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.A), protected : false });
		tools.CustomControl.addBinding(AimLeft, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.Q), protected : false });
		tools.CustomControl.addBinding(AimLeft, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.LEFT), protected : false });

		tools.CustomControl.addBinding(AimRight, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.D), protected : false });
		tools.CustomControl.addBinding(AimRight, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.RIGHT), protected : false });

		tools.CustomControl.addBinding(SelectAim, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.ENTER), protected : false });
	}
}
