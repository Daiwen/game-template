package systems;

class System {
    public static var ALL : Array<System> = [];
    public static var GC : Array<System> = [];

	public var destroyed(default,null) = false;
	public var tmod(get,never) : Float; inline function get_tmod() return Game.ME.tmod;

	public var cd : dn.Cooldown;

	public function new () {
		ALL.push(this);

		cd = new dn.Cooldown(Const.FPS);
	}

    public function dispose() {
        ALL.remove(this);

		cd.dispose();
		cd = null;
    }

    public function preUpdate() {
		cd.update(tmod);
	}

	public function update() {
	}

    public function postUpdate() {
	}
}
