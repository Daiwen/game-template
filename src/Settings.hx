class Settings {
	public static var particles = true;
	public static var shakes = true;
	public static var shakePower = 1.0;
	public static var animatedEffects = true;
	public static var speed = 1.0;
	public static var inputSuspend = 0.0;
	public static var assistedAiming = false;
	public static var gameFont = null;
	public static var uiFont = null;
	public static var textHints = true;
	public static var discrete = false;
	public static var sightless = false;

	public static function init() {
		gameFont = Assets.fontPixel;
		uiFont = Assets.fontMedium;

		discrete = sightless;
		assistedAiming = sightless;
	}
}
