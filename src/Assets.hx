import dn.heaps.slib.*;

class Assets {
	public static var noiseTexture : h3d.mat.Texture;

	public static var fontPixel : h2d.Font;
	public static var fontTiny : h2d.Font;
	public static var fontSmall : h2d.Font;
	public static var fontMedium : h2d.Font;
	public static var fontLarge : h2d.Font;
	public static var tiles : SpriteLib;
	public static var normals : SpriteLib;
	public static var sprites : SpriteLib;
	public static var hero : SpriteLib;
	public static var buttons : SpriteLib;

	public static var tilesDict = dn.heaps.assets.Aseprite.getDict(hxd.Res.world.tileset);
	public static var spritesDict = dn.heaps.assets.Aseprite.getDict(hxd.Res.atlas.sprites);
	public static var buttonsDict = dn.heaps.assets.Aseprite.getDict(hxd.Res.atlas.buttons);

	static var initDone = false;
	public static function init() {
		if( initDone )
			return;
		initDone = true;

		fontPixel = hxd.Res.fonts.minecraftiaOutline.toFont();
		fontTiny = hxd.Res.fonts.barlow_condensed_medium_regular_9.toFont();
		fontSmall = hxd.Res.fonts.barlow_condensed_medium_regular_11.toFont();
		fontMedium = hxd.Res.fonts.barlow_condensed_medium_regular_17.toFont();
		fontLarge = hxd.Res.fonts.barlow_condensed_medium_regular_32.toFont();

		tiles = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.world.tileset.toAseprite());
		normals = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.world.normals.toAseprite());
		sprites = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.atlas.sprites.toAseprite());
		hero = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.atlas.hero.toAseprite());
		buttons = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.atlas.buttons.toAseprite());

		noiseTexture = hxd.Res.noise2d.toTile().getTexture();
		noiseTexture.wrap = h3d.mat.Data.Wrap.Repeat;
		noiseTexture.filter = Nearest;
	}

	public static function getVolume( f ) {
		return ChucK.getFloat("volume", f);
	}

	public static function setVolume(v) {
		ChucK.setFloat("volume", v);
		ChucK.broadcastEvent("volumeChange");
	}

	public static function getMusicVolume( f ) {
		return ChucK.getFloat("mVolume", f);
	}

	public static function setMusicVolume(v) {
		ChucK.setFloat("mVolume", v);
		ChucK.broadcastEvent("volumeChange");
	}

	public static function getSoundEffectsVolume( f ) {
		return ChucK.getFloat("fVolume", f);
	}

	public static function setSoundEffectsVolume(v) {
		ChucK.setFloat("fVolume", v);
		ChucK.broadcastEvent("volumeChange");
	}

	public static function toggleMusicPause() {
	}
}
