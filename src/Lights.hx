import dn.Process;

class LightSource {
	public var x(get, set) : Float;
	inline function get_x() return data.x;
	inline function set_x(v : Float) {
		return data.x = v;
   	}
	public var y(get, set) : Float;
	inline function get_y() return data.y;
	inline function set_y(v : Float) {
		return data.y = v;
   	}
	public var radius(get, set) : Float;
	inline function get_radius() return data.z;
	inline function set_radius(v : Float) {
		var ret = data.z = v;
	   	drawLight();
	   	return ret;
	}
	public var wiggles(get, set) : Float;
	inline function get_wiggles() return data.w;
	inline function set_wiggles(v : Float) return data.w = v;
	public var data(default, null) : h3d.Vector4;
	var color : h3d.Vector4;

	public static var scene(default, null) : h2d.Scene;
	static var ambient(default, null) : h2d.Graphics;
	public var light : h2d.Graphics;
	public var visible(get, set) : Bool;
	inline function get_visible() return light.visible;
	inline function set_visible(v : Bool) return light.visible = v;

	static public function drawAmbient(w, h) {
		if (ambient != null) {
			ambient.remove();
		}

		ambient = new h2d.Graphics(scene);
		ambient.beginFill(0xffffff, 0.);
		ambient.drawRect(0, 0, w, h);
		ambient.endFill();
		ambient.blendMode = Add;
		ambient.visible = true;

		var shader = new vfx.shdr.AmbientLight();
		shader.setPriority(50);
		ambient.addShader(shader);
		shader.data = h3d.Vector4.fromArray([
				Math.cos(Math.PI/4) * Math.cos(Math.PI/4),
				Math.cos(Math.PI/4) * Math.sin(Math.PI/4),
				Math.sin(Math.PI/4),
				0.8 ]);
		shader.color = h3d.Vector4.fromColor(0xc6c65f);
		shader.color.w = 1;
	}

	static public function init(w, h) {
		scene = new h2d.Scene();
		new tools.RenderContext.LightRenderContext(scene);

		drawAmbient(w, h);
	}

	public function new(d : h3d.Vector4, c : Int) {
		color = h3d.Vector4.fromColor(c);
		data = d;

		radius = d.z;

		drawLight();
	}

	public function drawLight() {
		var visible = true;
		if (light != null) {
			light.remove();
			visible = light.visible;
		}

		light = new h2d.Graphics(scene);
		light.beginFill(0xffffff, 0.);
		light.drawCircle(0, 0, 4 * Const.GRID);
		light.endFill();
		light.blendMode = Add;
		light.visible = visible;

		var shader = new vfx.shdr.Light();
		shader.setPriority(50);
		light.addShader(shader);
		shader.data = data;
		shader.color = color;
		shader.color.w = 1;

		setPosition();
	}

	public function setColor(c:Int) {
		color.setColor(c);
		color.w = 1;
	}

	public function setPosition() {
		if (light == null)
			return;

		light.x = Game.ME.scroller.x + x;
		light.y = Game.ME.scroller.y + y;
	}

	public function resize() {
		scene.checkResize();

		drawLight();
	}

	public function destroy() {
		light.remove();
	}
}


class Lights extends Process {
	private var sources : Array<LightSource> = new Array();

	var colorTarget(get,never) : h3d.mat.Texture; inline function get_colorTarget() return Boot.ME.colorTarget;

	var scene : h2d.Scene;
	var bg : h2d.Bitmap;

	var lightTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
	var brightnessTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);

	public function new() {
		super(Game.ME);

		LightSource.init(engine.width, engine.height);

		scene = new h2d.Scene();
		new tools.RenderContext.BaseRenderContext(scene);

		bg = new h2d.Bitmap(h2d.Tile.fromTexture(colorTarget), scene);

		var shader = new vfx.shdr.AddLight(colorTarget, lightTarget, brightnessTarget);
		bg.addShader(shader);
	}

	public function addLightSource(s : LightSource) {
		sources.push(s);
	}

	public function removeLightSource(s : LightSource) {
		sources.remove(s);
		s.destroy();
	}

	public function render (e:h3d.Engine) {
		engine.pushTargets([lightTarget, brightnessTarget]);
		engine.clear(0, 1);
		if (LightSource.scene != null) {
			LightSource.scene.render(e);
		}
		engine.popTarget();

		scene.render(e);
	}

	override function onResize() {
		super.onResize();

		scene.checkResize();

		lightTarget.resize(engine.width, engine.height);
		brightnessTarget.resize(engine.width, engine.height);

		for (s in sources) {
			s.resize();
		}

		LightSource.drawAmbient(engine.width, engine.height);

		bg.tile.scaleToSize(engine.width, engine.height);
	}

	override function onDispose() {
		for (s in sources) {
			s.destroy();
		}
	}
}
