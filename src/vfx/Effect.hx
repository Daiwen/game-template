package vfx;

class Effect {
	public static var ALL : Array<Effect> = [];
	public static var GC : Array<Effect> = [];

	public var game(get,never) : Game; inline function get_game() return Game.ME;
	public var tmod(get,never) : Float; inline function get_tmod() return Game.ME.tmod;
	public var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;

	public var cd : dn.Cooldown;

	var root : h2d.Object;

	var x : Float;
	var y : Float;

	public var destroyed(default, null): Bool = false;

	public function new(o : h2d.Object, x : Float, y : Float, s : String) {
		ALL.push(this);

		root = new h2d.Object(o);

		game.repl.log(s);

		cd = new dn.Cooldown(Const.FPS);

		this.x = x;
		this.y = y;
	}

	public function update() {
		cd.update(tmod);

		root.x = x;
		root.y = y;
	}

	private function applyEffect() {
	}

	public function dispose() {
		ALL.remove(this);

		cd.dispose();
		cd = null;

		root.remove();
	}

    public inline function destroy() {
        if( !destroyed ) {
            destroyed = true;
            GC.push(this);
        }
    }
}
