class ProtoEntity {
    public static var ALL : Array<ProtoEntity> = [];

    public var cx = 0;
    public var cy = 0;
    public var xr = 0.5;
    public var yr = 0.5;

	var prefix(default, null) : Map<String, Bool> = new Map();
	var name : String;
	var suffix(default, null) : Map<String, Bool> = new Map();

	public function addSuffix(s : String) {
		suffix.set(s, true);
	}

	public function removeSuffix(s : String) {
		suffix.remove(s);
	}

	public function addPrefix(s : String) {
		prefix.set(s, true);
	}

	public function removePrefix(s : String) {
		prefix.remove(s);
	}

	public function new(s : String) {
		name = s;

		ALL.push(this);
	}

	public function destroy() {
		ALL.remove(this);
	}

	public function getName() {
		var prefixes = [ for (p in this.prefix.keys()) p ];
		var prefix = prefixes.join(" ");

		var suffixes = [ for (p in this.suffix.keys()) p ];
		var suffix = suffixes.join(" ");
	
		return prefix + " " + name + " " + suffix;
	}

	public function getLocatedName() {
		return getName() + " at [" + cx + "," + cy + "]";
	}

	dynamic public function getLog() {
		return getLocatedName();
	}

	dynamic public function details() {
	}
}
